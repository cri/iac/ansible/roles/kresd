# kresd (Knot Resolver)

## Sample usage

```
group_vars/kresd.yml
```
```yaml
kresd_listeners:
  - ip: 0.0.0.0
    port: 53
    opts:
      kind: dns
  - ip: 0.0.0.0
    port: 853
    opts:
      kind: tls
  - ip: "::"
    port: 53
    opts:
      kind: dns
      freebind: True
  - ip: "::"
    port: 853
    opts:
      kind: tls
      freebind: True

kresd_modules:
  - "hints > iterate" # Load /etc/hosts and allow custom root hints
  - "stats" # Track internal statistics
  - "predict" # Prefetch expiring/frequent records
  - "view"

kresd_additional_raw_config: |
  -- Cache size
  cache.size = 512 * MB

kresd_views:
  - tsig:
      - "5mykey"
    policies:
      - type: suffix
        action: pass
        suffix_table:
          todnames:
            - epita.fr
      - type: all
        action: deny
  - addr:
      - "10.224.32.0/24"
      - "10.224.33.0/24"
    policies:
      - type: suffix
        action: pass
        suffix_table:
          todnames:
            - epita.fr
            - epita.net
      - type: all
        action: deny

kresd_rpz:
  pie:
    zone_content: |
      $TTL 3600

      cri.epita.fr.			A	10.224.21.80

kresd_policies:
  - type: rpz
    action: deny
    rpz: pie
```

Playbook:

```yaml
- hosts: kresd
  roles:
    - cri.kresd
  tags:
    - kresd
    - dns
```

### Result

```
<config_dir>/kresd.conf
```
```
-- Knot Resolver (kresd) configuration -- don't edit manually!
--
-- Ansible managed

-- RPZ files
rpz_pie_zone_path = '/etc/knot-resolver/rpz/pie'

-- Network interface configuration
net.listen('0.0.0.0', 53, { kind = 'dns' })
net.listen('0.0.0.0', 853, { kind = 'tls' })
net.listen('::', 53, { kind = 'dns', freebind = true })
net.listen('::', 853, { kind = 'tls', freebind = true })


-- Load useful modules
modules = {
    'hints > iterate',
    'stats',
    'predict',
    'view',
}


-- Cache size
cache.size = 512 * MB



view:tsig('5mykey', policy.suffix(policy.PASS, policy.todnames({ 'epita.fr' })))
view:tsig('5mykey', policy.all(policy.DENY))
view:addr('10.224.32.0/24', policy.suffix(policy.PASS, policy.todnames({ 'epita.fr', 'epita.net' })))
view:addr('10.224.32.0/24', policy.all(policy.DENY))
view:addr('10.224.33.0/24', policy.suffix(policy.PASS, policy.todnames({ 'epita.fr', 'epita.net' })))
view:addr('10.224.33.0/24', policy.all(policy.DENY))


policy.add(policy.rpz(policy.DENY, rpz_pie_zone_path))
```

```
<config_rpz_dir>/pie
```
```
$TTL 3600

cri.epita.fr.                   A       10.224.21.80
```
