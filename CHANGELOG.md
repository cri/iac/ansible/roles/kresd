# Changelog

All notable changes to this project will be documented in this file.

## [unreleased] - since 0.1.0

## [0.1.0] - 2022-10-12

### Added

- Install kresd from CZ-NIC repository
- Basic configuration options: listeners, policies, views, modules and rpz

[0.1.0]: https://gitlab.cri.epita.fr/cri/iac/ansible/roles/kresd/-/releases/0.1.0
